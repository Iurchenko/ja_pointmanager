package com.ort.strazhkoe.entities;

import java.io.Serializable;

public class Point implements Serializable {

	private static final long serialVersionUID = 2945844683652803371L;
	
	private  int _x;
	private  int _y;
	private  String _name;
	
	public void setX(int _x) {
        this._x = _x;
    }

    public void setY(int _y) {
        this._y = _y;
    }

    public void setName(String _name) {
        this._name = _name;
    }


	public int getX() {
		return _x;
	}

	public int getY() {
		return _y;
	}

	public String getName() {
		return _name;
	}
	
    public Point() {}

    public Point(final int x, 
            final int y, 
            final String name) {
   _x = x;
   _y = y;
   _name = name;
}

	public String toString() {
		return String.format("%s (%d, %d)", _name, _x, _y);
	}
}
