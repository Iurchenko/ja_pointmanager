package com.ort.strazhkoe;

import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.dao.impl.FilePointDAO;
import com.ort.strazhkoe.dao.impl.SerializeFilePointDAO;
import com.ort.strazhkoe.services.converters.CSVPointConverter;

public enum PointDAOKeys {
	FILE_DAO("fd"),
	SERIALIZABLE_FILE_DAO("sfd");
	
	private String _key;
	
	PointDAOKeys(String key) {
		_key = key;
	}
	
	public PointDAO getDAO(String fileName) {
		switch(_key) {
		case "fd":
			return new FilePointDAO(fileName, new CSVPointConverter());
		case "sfd":
			return new SerializeFilePointDAO(fileName);
		}
		return null;
	}
	
	public String getKey() {
		return _key;
	}
}
