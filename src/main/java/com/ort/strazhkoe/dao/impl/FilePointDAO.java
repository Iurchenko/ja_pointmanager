package com.ort.strazhkoe.dao.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.entities.Point;
import com.ort.strazhkoe.services.converters.PointConverter;

public class FilePointDAO implements PointDAO {

	private final String _fileName;
	private final PointConverter _pointConverter;

	public FilePointDAO(String fileName, PointConverter pointConverter) {
		_fileName = fileName;
		_pointConverter = pointConverter;
	}

	@Override
	public List<Point> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveAll(List<Point> points) {
		try (PrintWriter writer = getWriter()) {
			points.stream()
			      .map(_pointConverter::toString)
			      .forEach(writer::println);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	protected PrintWriter getWriter() throws IOException {
		return new PrintWriter(new FileWriter(_fileName));
	}
}
