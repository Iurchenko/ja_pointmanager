package com.ort.strazhkoe.dao.impl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.List;

import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.entities.Point;

public class SerializeFilePointDAO implements PointDAO {

	private final String _fileName;

	public SerializeFilePointDAO(String fileName) {
		_fileName = fileName;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Point> getAll() {
		try (ObjectInputStream ois =
				new ObjectInputStream(new FileInputStream(_fileName))) {
			return (List<Point>) ois.readObject();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		
		return Collections.emptyList();
	}

	@Override
	public void saveAll(List<Point> points) {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(_fileName))) {

			oos.writeObject(points);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
