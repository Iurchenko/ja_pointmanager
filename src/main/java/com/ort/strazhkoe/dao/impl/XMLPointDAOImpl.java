package com.ort.strazhkoe.dao.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.entities.Point;

public class XMLPointDAOImpl implements PointDAO {
    File xmlFile = new File("points.xml");
    
    public XMLPointDAOImpl() { 
    }
    public XMLPointDAOImpl(File file) {
        this.xmlFile = file;
    }
    @Override
    public List<Point> getAll() throws ParserConfigurationException, SAXException, IOException {
        List<Point> listPoint = new ArrayList<Point>();
        
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = (Document) dBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();

        NodeList nodeList = doc.getElementsByTagName("block");

        for (int temp = 0; temp < nodeList.getLength(); temp++) {
            Point point = new Point();
            Node nNode = nodeList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;

                int x = Integer.parseInt(eElement.getElementsByTagName("x").item(0).getTextContent()); 
                point.setX(x);
                int y = Integer.parseInt(eElement.getElementsByTagName("y").item(0).getTextContent()); 
                point.setY(y);
                point.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                listPoint.add(point);
            }
        }
        return listPoint;
    }

    @Override
    public void saveAll(List<Point> points) {

    }
}
