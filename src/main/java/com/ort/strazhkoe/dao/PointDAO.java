package com.ort.strazhkoe.dao;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ort.strazhkoe.entities.Point;

public interface PointDAO {

	public List<Point> getAll() throws ParserConfigurationException, SAXException, IOException;
	
	public void saveAll(List<Point> points);
}
