package com.ort.strazhkoe.ui.swing;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.ort.strazhkoe.entities.Point;
import com.ort.strazhkoe.services.PointService;
import com.ort.strazhkoe.ui.UI;

public class SwingUI implements UI {

    private DefaultListModel<Point> _listModel;
    private PointService _pointService;
    ErrorWindow errorWindow;
    JList<Point> jList;
    Messages message;
    JFrame jFrame;

    // Create menu bar
    JMenuBar menuBar = new JMenuBar();
    JMenu menuAction = new JMenu("Operation");
    JMenu menuDestination = new JMenu("Load");
    JMenu loadFromXMLMenu = new JMenu("from XML");
    JMenu conventionInfo = new JMenu("Convention");
    JMenu menuLocale = new JMenu("Language");

    // Create menuItems
    JMenuItem newMenu = new JMenuItem("New point");
    JMenuItem editMenu = new JMenuItem("Edit point");
    JMenuItem deleteMenu = new JMenuItem("Delete point");
    JMenuItem clearMenu = new JMenuItem("Clear");
    JMenuItem exitMenu = new JMenuItem("Exit");
    JMenuItem information = new JMenuItem("Information");

    JMenuItem loadFromCSVMenu = new JMenuItem("from CSV");
    JMenuItem fromSAXmenu = new JMenuItem("Use SAX parser");
    JMenuItem fromDOMmenu = new JMenuItem("Use DOM parser");
    JMenuItem fromJAXBmenu = new JMenuItem("Use JAXB parser");

    JMenuItem ruLanguage = new JMenuItem("Russian");
    JMenuItem enLanguage = new JMenuItem("English");
    JMenuItem frLanguage = new JMenuItem("France");

    JButton addButton = new JButton("Add Point");
    JButton deleteButton = new JButton("Delete Point(s)");

    public SwingUI(PointService pointService) {
        _pointService = pointService;
    }

    @Override
    public void start(String[] args) {
        jFrame = new JFrame("Point Manager");
        jFrame.setSize(400, 600);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setLocationRelativeTo(null);
        this.headMenuLoader(jFrame); /////////////////// our new menu///////////////////////

        _listModel = new DefaultListModel<>();
        refresh();
        jList = new JList<>(_listModel);
        jList.setFont(new Font("Serif", Font.PLAIN, 28));

        // Create button panel and add buttons
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(addButton);
        buttonPanel.add(deleteButton);

        // Listeners for Buttons
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddPointDialog addPointDialog = new AddPointDialog(jFrame, _pointService);
                addPointDialog.setVisible(true);
                refresh();
            }
        });

        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jList.getSelectedValue() != null) {
                    _pointService.delete(jList.getSelectedValue());
                    refresh();
                } else if (_listModel.size() != 0) {
                    showError(message.ERROR_DELETE);
                } else {
                    showError(message.ERROR_NOT_FOUND);
                }
            }
        });
        jFrame.add(jList, BorderLayout.CENTER);
        jFrame.add(buttonPanel, BorderLayout.SOUTH);
        jFrame.setVisible(true);
    }

    /**
     * Метод загружает локали из пропертей в зависимости от выбраного языка в меню
     * @param language = язык(FR,RU,EN)
     */
    private void setLanguage(String language) {
        ResourceBundle rb = null;
        if (language == "RU") {
            rb = ResourceBundle.getBundle("lang_ru");
        }
        if (language == "EN") {
            rb = ResourceBundle.getBundle("lang_en");
        }
        if (language == "FR") {
            rb = ResourceBundle.getBundle("lang_fr");
        }
        
        menuAction.setText(rb.getString(MenuItemsName.MENU_BAR_OPERATION));
        menuDestination.setText(rb.getString(MenuItemsName.MENU_BAR_LOAD));
        conventionInfo.setText(rb.getString(MenuItemsName.MENU_BAR_CONVENTION));
        menuLocale.setText(rb.getString(MenuItemsName.MENU_BAR_LANGUAGE));

        newMenu.setText(rb.getString(MenuItemsName.MENU_ACTION_NEW));
        editMenu.setText(rb.getString(MenuItemsName.MENU_ACTION_EDIT));
        deleteMenu.setText(rb.getString(MenuItemsName.MENU_ACTION_DELETE));
        clearMenu.setText(rb.getString(MenuItemsName.MENU_ACTION_CLEAR));
        exitMenu.setText(rb.getString(MenuItemsName.MENU_ACTION_EXIT));

        loadFromCSVMenu.setText(rb.getString(MenuItemsName.MENU_LOAD_FROMCSV));
        loadFromXMLMenu.setText(rb.getString(MenuItemsName.MENU_LOAD_FROMXML));
        fromSAXmenu.setText(rb.getString(MenuItemsName.MENU_LOAD_FROMXML_SAX));
        fromJAXBmenu.setText(rb.getString(MenuItemsName.MENU_LOAD_FROMXML_JAXB));
        fromDOMmenu.setText(rb.getString(MenuItemsName.MENU_LOAD_FROMXML_DOM));

        ruLanguage.setText(rb.getString(MenuItemsName.MENU_LANGUAGE_RU));
        enLanguage.setText(rb.getString(MenuItemsName.MENU_LANGUAGE_EN));
        frLanguage.setText(rb.getString(MenuItemsName.MENU_LANGUAGE_FR));

        addButton.setText(rb.getString(MenuItemsName.BUTTON_ADD));
        deleteButton.setText(rb.getString(MenuItemsName.BUTTON_DELETE));
    }

    protected void refresh() {
        _listModel.clear();
        _pointService.getAll().stream().forEach(point -> _listModel.addElement(point));
    }

    public void showError(Messages message) {
        errorWindow = new ErrorWindow(jFrame, message);
        errorWindow.setVisible(true);
    }

    // Menu loader
    public void headMenuLoader(JFrame frame) {

        // Add menuItem to menu
        menuAction.add(newMenu);
        menuAction.add(editMenu);
        menuAction.add(deleteMenu);
        menuAction.add(clearMenu);
        menuAction.addSeparator();
        menuAction.add(exitMenu);

        loadFromXMLMenu.add(fromSAXmenu);
        loadFromXMLMenu.add(fromDOMmenu);
        loadFromXMLMenu.add(fromJAXBmenu);

        menuDestination.add(loadFromXMLMenu);
        menuDestination.add(loadFromCSVMenu);
        
        conventionInfo.add(information);

        menuLocale.add(ruLanguage);
        menuLocale.add(enLanguage);
        menuLocale.add(frLanguage);

        // Add menu to menuBar
        menuBar.add(menuAction);
        menuBar.add(menuDestination);
        menuBar.add(conventionInfo);
        menuBar.add(menuLocale);

        // Set menuBar on frame
        frame.setJMenuBar(menuBar);

        // Listeners
        ruLanguage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLanguage("RU");

            }
        });

        enLanguage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLanguage("EN");
            }
        });
        frLanguage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLanguage("FR");
            }
        });

        exitMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        newMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddPointDialog addPointDialog = new AddPointDialog(frame, _pointService);
                addPointDialog.setVisible(true);
                refresh();
            }
        });

        editMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jList.getSelectedValue() != null) {
                    Point propertyPoint = jList.getSelectedValue();
                    EditPointDialog editPointDialog = new EditPointDialog(frame, _pointService, propertyPoint);
                    editPointDialog.setVisible(true);
                    refresh();
                } else if (_listModel.size() != 0) {
                    showError(message.ERROR_EDIT);
                } else {
                    showError(message.ERROR_NOT_FOUND);
                }
            }
        });

        deleteMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (jList.getSelectedValue() != null) {
                    _pointService.delete(jList.getSelectedValue());
                    refresh();
                } else if (_listModel.size() != 0) {
                    showError(message.ERROR_DELETE);
                } else {
                    showError(message.ERROR_NOT_FOUND);
                }
            }
        });

        clearMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                _pointService.clear();
                refresh();
            }
        });
        
        loadFromCSVMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser fileopen = new JFileChooser();
                int option = fileopen.showDialog(null, "Open file");
                if (option == JFileChooser.APPROVE_OPTION) {
                    try {
                        _pointService.clear();
                        _pointService.loadFromFileChooserCSV(fileopen.getSelectedFile());
                        refresh();
                    } catch (IOException e1) {
                    }
                } else {
                    showError(message.ERROR_CHOOSE_FILE);
                }
            }
        });
    }
}
