package com.ort.strazhkoe.ui.swing;

public interface MenuItemsName {
            String MENU_BAR_OPERATION="menuBar.Operation";
            String MENU_BAR_LOAD="menuBar.Load";
            String MENU_BAR_CONVENTION="menuBar.Convention";
            String MENU_BAR_LANGUAGE="menuBar.Language";

            String MENU_ACTION_NEW="menuAction.New";
            String MENU_ACTION_EDIT="menuAction.Edit";
            String MENU_ACTION_DELETE="menuAction.Delete";
            String MENU_ACTION_CLEAR="menuAction.Clear";
            String MENU_ACTION_EXIT="menuAction.Exit";

            String MENU_LOAD_FROMCSV="menuLoad.FromCSV";
            String MENU_LOAD_FROMXML="menuLoad.FromXML";
            
            String MENU_LOAD_FROMXML_SAX="menuLoad.FromXML.Sax";
            String MENU_LOAD_FROMXML_JAXB="menuLoad.FromXML.Jax";
            String MENU_LOAD_FROMXML_DOM="menuLoad.FromXML.Dom";

            String MENU_LANGUAGE_RU="menuLanguageRU";
            String MENU_LANGUAGE_EN="menuLanguageEN";
            String MENU_LANGUAGE_FR="menuLanguageFR";
            
            String BUTTON_ADD="buttonAdd";
            String BUTTON_DELETE="buttonDelete";
}
