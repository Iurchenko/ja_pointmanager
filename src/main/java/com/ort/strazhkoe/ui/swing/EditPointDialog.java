
package com.ort.strazhkoe.ui.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ort.strazhkoe.entities.Point;
import com.ort.strazhkoe.services.PointService;

public class EditPointDialog extends JDialog {

    private static final long serialVersionUID = 4274820258900984730L;
    private final PointService _pointService;
    private JTextField _xField;
    private JTextField _yField;
    private JTextField _nameField;
    private Point _point;

    public EditPointDialog(JFrame parent, PointService pointService, Point point) {
                super(parent);
                _pointService = pointService;
                _point = point;

                setTitle("Enter new value...");
                setModal(true);
                setResizable(false);
                
                //Create label
                JLabel oldLabel = new JLabel("Old value:");
                JLabel oldValueLabel = new JLabel(point.toString());
                JLabel xLabel = new JLabel("X:");
                _xField = new JTextField();
                _xField.setText("");
                _xField.setPreferredSize(new Dimension(50, 20));
                JLabel yLabel = new JLabel("Y:");
                _yField = new JTextField();
                _yField.setPreferredSize(new Dimension(50, 20));
                JLabel nameLabel = new JLabel("Name:");
                _nameField = new JTextField();
                _nameField.setPreferredSize(new Dimension(50, 20));

                //Add field and label into panels
                JPanel inputPanel = new JPanel();
                JPanel outputPanel = new JPanel();
                inputPanel.setBorder(BorderFactory.createLineBorder(Color.BLUE));
                inputPanel.setLayout(new FlowLayout());
                outputPanel.add(oldLabel);
                outputPanel.add(oldValueLabel);
                inputPanel.add(xLabel);
                inputPanel.add(_xField);
                inputPanel.add(yLabel);
                inputPanel.add(_yField);
                inputPanel.add(nameLabel);
                inputPanel.add(_nameField);

                //Create button internal panel
                JPanel buttonPanel = new JPanel();
                JButton okButton = new JButton("Save");
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                            if (_xField.getText().trim().length() > 0 
                                && _yField.getText().trim().length() > 0
                                && _nameField.getText().trim().length() > 0) { // ???
                            ok();
                            closeDialog();

                        } else {
                            ErrorWindow attention = new ErrorWindow(parent, Messages.ERROR_VALUE);
                            attention.setVisible(true);
                            closeDialog();
                        }
                    }
                });
                
                JButton cancelButton = new JButton("Cancel");
                cancelButton.addActionListener(e -> closeDialog());

                buttonPanel.add(okButton);
                buttonPanel.add(cancelButton);

                //Choose position for panel 
                getContentPane().add(outputPanel, BorderLayout.NORTH);
                getContentPane().add(inputPanel, BorderLayout.CENTER);
                getContentPane().add(buttonPanel, BorderLayout.SOUTH);

                setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                pack();
                setLocationRelativeTo(parent);
            }

    private void closeDialog() {
        setVisible(false);
        dispose();
    }

    public void ok() {
        _point.setX(Integer.parseInt(_xField.getText().trim()));
        _point.setY(Integer.parseInt(_yField.getText().trim()));
        _point.setName(_nameField.getText().trim());
    }
}
