package com.ort.strazhkoe.ui.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ort.strazhkoe.entities.Point;
import com.ort.strazhkoe.services.PointService;

public class ErrorWindow extends JDialog {

    private static final long serialVersionUID = -129981823200667120L;
    private Messages _message;

    public ErrorWindow(JFrame parent, Messages message) {
        super(parent);
        _message = message;

        setTitle("Error!");
        setModal(true);
        setResizable(false);
        

        JLabel infoLabel = new JLabel(message.getTitle());
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new FlowLayout());
        inputPanel.add(infoLabel);

        JPanel buttonPanel = new JPanel();
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
        });

        buttonPanel.add(okButton);
        getContentPane().add(inputPanel, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(parent);
    }

    private void closeDialog() {
        setVisible(false);
        dispose();
    }
    
    public String getMessage() {
        return _message.toString();
    }
}
