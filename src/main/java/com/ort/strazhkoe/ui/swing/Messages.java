package com.ort.strazhkoe.ui.swing;

public enum  Messages {
    ERROR_DELETE ("Please, choose point from JList"),
    ERROR_EDIT ("Please, choose point from JList"),
    ERROR_ADD ("Please, choose point from JList"),
    ERROR_VALUE ("Incorrect value"),
    ERROR_CHOOSE_FILE ("File was not selected"),
    ERROR_NOT_FOUND ("JList is clear");

    private String title;

    Messages(String title) {
        this.title = title;
    }
    
    public String getTitle() {
        return title;
    }
}
