package com.ort.strazhkoe.ui.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.ort.strazhkoe.entities.Point;
import com.ort.strazhkoe.services.PointService;

public class AddPointDialog extends JDialog {

    private static final long serialVersionUID = 4274820258900984730L;
    private final PointService _pointService;
    private JTextField _xField;
    private JTextField _yField;
    private JTextField _nameField;

    public AddPointDialog(JFrame parent, PointService pointService) {
        super(parent);
        _pointService = pointService;
        setResizable(false);
        setTitle("Add Point");
        setModal(true);
        JLabel xLabel = new JLabel("X:");
        _xField = new JTextField();
        _xField.setText("");
        _xField.setPreferredSize(new Dimension(50, 20));
        JLabel yLabel = new JLabel("Y:");
        _yField = new JTextField();
        _yField.setPreferredSize(new Dimension(50, 20));
        JLabel nameLabel = new JLabel("Name:");
        _nameField = new JTextField();
        _nameField.setPreferredSize(new Dimension(50, 20));

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new FlowLayout());
        inputPanel.add(xLabel);
        inputPanel.add(_xField);
        inputPanel.add(yLabel);
        inputPanel.add(_yField);
        inputPanel.add(nameLabel);
        inputPanel.add(_nameField);

        JPanel buttonPanel = new JPanel();
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (_xField.getText().trim().length() > 0 && _yField.getText().trim().length() > 0
                        && _nameField.getText().trim().length() > 0) { // ???
                    ok();
                    closeDialog();

                } else {
                    ErrorWindow attention = new ErrorWindow(parent, Messages.ERROR_VALUE);
                    attention.setVisible(true);
                    closeDialog();
                }
            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> closeDialog());

        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        getContentPane().add(inputPanel, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(parent);
    }

    private void closeDialog() {
        setVisible(false);
        dispose();
    }

    public void ok() {
        int xVal = Integer.parseInt(_xField.getText().trim());
        int yVal = Integer.parseInt(_yField.getText().trim());
        String name = _nameField.getText().trim();
        Point p = new Point(xVal, yVal, name);
        _pointService.add(p);
    }
}
