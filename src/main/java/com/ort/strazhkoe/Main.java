package com.ort.strazhkoe;

import java.io.IOException;
import java.util.Arrays;
import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.dao.impl.FilePointDAO;
import com.ort.strazhkoe.dao.impl.SerializeFilePointDAO;
import com.ort.strazhkoe.dao.impl.XMLPointDAOImpl;
import com.ort.strazhkoe.services.PointServiceImpl;
import com.ort.strazhkoe.services.converters.CSVPointConverter;
import com.ort.strazhkoe.services.converters.PointConverter;
import com.ort.strazhkoe.ui.UI;
import com.ort.strazhkoe.ui.swing.SwingUI;

public class Main {


        
    private static final String DEFAULT_FILE_PATH = "points.csv";

    public static void main(String[] args) throws IOException {
        PointDAO pointDAO = createPointDAO(args);
        PointServiceImpl pointService = new PointServiceImpl(pointDAO);
        UI ui = new SwingUI(pointService);
        // ********* Loading
        ui.start(args);
    }

    private static PointDAO createPointDAO(String[] args) {
        if (args.length >= 2) {
            return Arrays.stream(PointDAOKeys.values()).filter(key -> key.getKey().equals(args[0])).findAny()
                    .orElse(PointDAOKeys.FILE_DAO).getDAO(args[1]);
        }

        return getDefaultPointDAO(DEFAULT_FILE_PATH);
    }

    private static PointDAO getDefaultPointDAO(String pathToFile) {
        PointConverter converter = new CSVPointConverter();
        return new FilePointDAO(pathToFile, converter);
    }
    
}
