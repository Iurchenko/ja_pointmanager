package com.ort.strazhkoe.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ort.strazhkoe.dao.PointDAO;
import com.ort.strazhkoe.dao.impl.XMLPointDAOImpl;
import com.ort.strazhkoe.entities.Point;

public class PointServiceImpl implements PointService {

    private final PointDAO _pointDAO;
    private final List<Point> _data;

    public PointServiceImpl(final PointDAO pointDAO) throws IOException {
        _pointDAO = pointDAO;
        _data = new ArrayList<>();
        this.loadPointFromFile();
    }

    @Override
    public List<Point> getAll() {
        return Collections.unmodifiableList(_data);
    }

    @Override
    public void saveAll(List<Point> points) {
        _pointDAO.saveAll(points);
    }

    @Override
    public void add(Point point) {
        _data.add(point);
        save();
    }

    @Override
    public void delete(int index) {
        _data.remove(index);
        save();
    }

    @Override
    public void delete(Point point) {
        _data.remove(point);
        save();
    }

    private void save() {
        _pointDAO.saveAll(_data);
    }

    @Override
    public void loadPointFromFile() throws IOException {
        BufferedReader fileRead = null;
        String str = null;
        fileRead = new BufferedReader(new FileReader("points.csv"));

        while ((str = fileRead.readLine()) != null) {
            Point testPoint = new Point();
            _data.add(findKey(str, testPoint));
        }
        fileRead.close();
    }

    public Point findKey(String s, Point point) {
        String patternY = "(;\\d+;)";
        String patternX = "(\\d+;)";
        String patternValue = "(\\d+)";
        String patternName = "(;\\D+)";
        String patternValueName = "(\\w+)";

        point.setX(Integer.parseInt(find(s, patternX, patternValue)));
        point.setY(Integer.parseInt(find(s, patternY, patternValue)));
        point.setName(find(s, patternName, patternValueName));

        return point;
    }

    private String find(String s, String pattern, String patternValue) {
        String result = "";
        String value = "";

        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(s);
        if (m.find())
            result = m.group(0).toString();
        r = Pattern.compile(patternValue, Pattern.CASE_INSENSITIVE);
        m = r.matcher(result);
        if (m.find())
            value = m.group(0);
        return value;
    }

    @Override
    public void loadFromFileChooserCSV(File fr) throws IOException {
        BufferedReader fileRead = null;
        String str = null;
        fileRead = new BufferedReader(new FileReader(fr));

        while ((str = fileRead.readLine()) != null) {
            Point testPoint = new Point();
            _data.add(findKey(str, testPoint));
        }
        fileRead.close();

    }

    @Override
    public void clear() {
        _data.clear();
    }

    @Override
    public void loadFromFileChooserXML(File fr) throws IOException, ParserConfigurationException, SAXException {
        XMLPointDAOImpl xmlRead = new XMLPointDAOImpl(fr);
        
        for(Point x:xmlRead.getAll())
        _data.add(x);
    }
}
