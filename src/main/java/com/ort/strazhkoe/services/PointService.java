package com.ort.strazhkoe.services;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ort.strazhkoe.entities.Point;

public interface PointService {
	
	public List<Point> getAll();
	
	public void saveAll(List<Point> points);

	public void add(Point point);
	
	public void delete(int index);
	
	public void delete(Point point);
	
	public void clear();
	
	public void loadPointFromFile() throws IOException;
	
	public void loadFromFileChooserCSV(File fr) throws IOException;
	public void loadFromFileChooserXML(File fr) throws IOException, ParserConfigurationException, SAXException;
        
}
