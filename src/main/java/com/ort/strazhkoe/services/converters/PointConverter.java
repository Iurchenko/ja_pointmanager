package com.ort.strazhkoe.services.converters;

import com.ort.strazhkoe.entities.Point;

public interface PointConverter {
	Point toPoint(String text);
	String toString(Point point);
}
