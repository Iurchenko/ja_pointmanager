package com.ort.strazhkoe.services.converters;

import com.ort.strazhkoe.entities.Point;

public class CSVPointConverter implements PointConverter {

	private String SEPARATOR = ";";

	@Override
	public Point toPoint(String text) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toString(Point point) {
		StringBuilder sb = new StringBuilder();
		sb.append(point.getX())  
		  .append(SEPARATOR)
		  .append(point.getY())
		  .append(SEPARATOR)
		  .append(point.getName());
		
		return sb.toString();
	}

	
}
