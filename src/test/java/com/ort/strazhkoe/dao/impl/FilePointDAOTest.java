package com.ort.strazhkoe.dao.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.ort.strazhkoe.entities.Point;
import com.ort.strazhkoe.services.converters.PointConverter;


public class FilePointDAOTest {

	private List<Point> _data = Arrays.asList(
			new Point(1,2,"A"),
			new Point(7,5,"B")
		);

	@Test
	public void testSaveAll() {
		
		PointConverter pointConverterMock = mock(PointConverter.class);
		when(pointConverterMock.toString(any(Point.class))).thenReturn("fake");
		
		PrintWriter printWriterMock = mock(PrintWriter.class);
		
		
		FilePointDAO fpd = new FilePointDAO(null, pointConverterMock) {
			@Override
			protected PrintWriter getWriter() throws IOException {
				return printWriterMock;
			}
		};
		
		fpd.saveAll(_data);
		verify(printWriterMock, times(2)).println("fake");
	}
	
	@Test(expected = RuntimeException.class)
	public void testSaveAllRuntimeException() {
		PointConverter pointConverterMock = mock(PointConverter.class);
		when(pointConverterMock.toString(any(Point.class))).thenReturn("fake");

		// throw exception on method call
//		PrintWriter printWriterMock = mock(PrintWriter.class);
//		doThrow(new IOException()).when(printWriterMock).println("fake");
		
		FilePointDAO fpd = new FilePointDAO(null, pointConverterMock) {
			@Override
			protected PrintWriter getWriter() throws IOException {
				throw new IOException();
			}
		};
		
		fpd.saveAll(_data);
	}
}
